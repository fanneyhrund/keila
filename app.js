angular.module("keilumaster300", ['ngMaterial'])
.controller("appController",
    function($scope, $anchorScroll, $location){
        $scope.maxNum = 0;
        $scope.done = [];

        $scope.newNum = function(){
          if ($scope.done.length !== $scope.maxNum) {
            var x = Math.floor(Math.random() * $scope.maxNum)+1;
            $location.hash('anchor' + x);
            if($scope.done.indexOf(x) === -1){
                $scope.done.push(x);
            }
            else {
              $scope.newNum();
            }
          }
        };
    });
